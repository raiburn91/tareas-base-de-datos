-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-07-2020 a las 23:28:54
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_restricciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inmuebles`
--

CREATE TABLE `inmuebles` (
  `id` int(11) NOT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `idtipoimueble` int(6) DEFAULT 0,
  `precio` decimal(10,2) DEFAULT 0.00,
  `comision` decimal(10,0) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `idprovincia` int(10) DEFAULT NULL,
  `idlocalidad` int(10) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `pisoydepto` varchar(100) DEFAULT NULL,
  `entre_calles` text DEFAULT NULL,
  `idoperacion` int(100) DEFAULT NULL,
  `destacado` char(3) DEFAULT 'no',
  `imagen1` varchar(255) DEFAULT NULL,
  `imagen2` varchar(255) DEFAULT NULL,
  `imagen3` varchar(255) DEFAULT NULL,
  `imagen4` varchar(255) DEFAULT NULL,
  `antiguedad` varchar(100) DEFAULT NULL,
  `mt2cubiertos` int(11) DEFAULT NULL,
  `superficie_lote` int(11) DEFAULT NULL,
  `activado` enum('si','no') NOT NULL DEFAULT 'si'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `inmuebles`
--
ALTER TABLE `inmuebles`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
