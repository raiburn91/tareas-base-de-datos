Conceptos básicos del modelo relacional
Modelo relacional: modelo de organización y gestión de bases de datos consistente en el almacenamiento de datos en tablas compuestas por filas, o tuplas, y columnas o campos. Se distingue de otros modelos, como el jerárquico, por ser más comprensible para el usuario inexperto, y por basarse en la lógica de predicados para establecer relaciones entre distintos datos. Surge como solución a la creciente variedad de los datos que integran las data warehouses y podemos resumir el concepto como una colección de tablas (relaciones).
 
Tabla: es el nombre que recibe cada una de las relaciones que se establecen entre los datos almacenados; cada nueva relación da lugar a una tabla. Están formadas por filas, también llamadas tuplas, donde se describen los elementos que configuran la tabla (es decir, los elementos de la relación establecida por la tabla), columnas o campos, con los atributos y valores correspondientes, y el dominio, concepto que agrupa a todos los valores que pueden figurar en cada columna.

Claves: elementos que impiden la duplicidad de registros, una de las grandes desventajas que presentan otros modelos de organización y gestión de bases de datos. Existen dos grandes tipos de claves: las claves primarias y las secundarias o externas.
 
Claves primarias: son los atributos (columnas) según el tipo de relación que se ha definido en la tabla. Pueden añadirse otros atributos específicos y propios. 
 
Claves externas o secundarias: son las claves que se definen para cada una de las claves primarias establecidas para los elementos o entidades de una relación.

Restricción de identidad: límites y restricciones que se imponen en las relaciones, imprescindibles para mantener la significación correcta de la base de datos. Es un concepto íntimamente vinculado a las reglas de integridad propias del modelo relacional, el cumplimiento de las cuales está garantizado por las claves primarias y externas.
Existen 4 tipos básicos de restricciones de integridad:
los datos requeridos (los campos o columnas siempre deben poseer un atributo o un valor)
la comprobación de validez (las tablas deben contener solo los datos correspondientes a la correspondiente relación definida por cada tabla)
las integridades de entidad y referencial (las primeras aseguran que las claves primarias posean un valor único para cada tupla, y las segundas que las claves principales y las externas mantengan su integridad)
 
Reglas de integridad: reglas que garantizan la integridad de los datos, es decir, la correspondencia plausible de los datos con la realidad.
https://blog.es.logicalis.com/analytics/conceptos-basicos-del-modelo-relacional-en-la-gestion-de-bases-de-datos
