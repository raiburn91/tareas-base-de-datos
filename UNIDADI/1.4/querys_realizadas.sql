#mostrar el numero de ventas DESCe cada producto
SELECT codigo, nombre, COUNT(venta.producto)
  FROM producto LEFT JOIN venta 
  ON producto.codigo = venta.producto
  GROUP BY codigo, nombre ORDER BY COUNT(venta.producto) DESC

#obtener las ventas realizadas en cada piso
SELECT nom_apels, nombre, precio, piso
  FROM cajeros c INNER JOIN
  (producto p INNER JOIN
    (maquinas_registradoras m INNER JOIN venta v on v.maquina = m.codigo)
      ON v.producto = p.codigo)
      On v.cajero = c.codigo;
#o puede ser así
SELECT nom_apels, nombre, precio, piso
  FROM venta v, cajeros c, productos p, maquinas_registradoras m  WHERE
    v.cajero = c.codigo
    AND c.producto = p.codigo
    AND v.maquina = m.codigo

SELECT piso, SUM(precio)
  FROM venta v, productos p, maquinas_registradoras m
  WHERE v.productos = p.codigo
  AND v.maquina = m.codigo
  GROUP BY piso
#codigo ynombre de cada empleado junto con el importe de sus ventas totales
SELECT c.codigo, c.nom_apels, SUM(precio)
  FROM productos p INNER JOIN 
  (cajeros c LEFT JOIN venta v
    ON v.cajero = c.codigo)
  ON v.producto = p.codigo
  GROUP BY c.codigo, nom_apels
  #Obtener el codigo y nombre de aquellos que hayan realizado ventas en pisos cuyas 
  #ventas totales sean inferiores a los 500 ea
  SELECT codigo, nom_apels
  FROM cajeros WHERE codigo IN
    (SELECT cajero FROM venta WHERE maquina IN
      (
        SELECT codigo FROM maquinas_registradoras
        WHERE piso IN
        (SELECT piso 
          FROM venta v, productos p, maquinas_registradoras m WHERE v.producto = p.codigo
          GROUP BY piso
          HAVING SUM(precio)<500)
        )
      )

#tablas
create table libros(
  codigo int identity,
  titulo varchar(40),
  autor varchar(30) default 'Desconocido',
  codigoeditorial tinyint not null,
  precio decimal(5,2)
 );
 create table editoriales(
  codigo tinyint identity,
  nombre varchar(20),
  primary key (codigo)
);
 #registros
insert into editoriales values('Planeta');
 insert into editoriales values('Emece');
 insert into editoriales values('Siglo XXI');
 insert into libros values('El aleph','Borges',2,20);
 insert into libros values('Martin Fierro','Jose Hernandez',1,30);
 insert into libros values('Aprenda PHP','Mario Molina',3,50);
 insert into libros values('Java en 10 minutos',default,3,45);
#joins
 select titulo, autor, nombre
  from libros
  join editoriales
  on codigoeditorial=editoriales.codigo;


  select l.codigo,titulo,autor,nombre,precio
  from libros as l
  join editoriales as e
  on codigoeditorial=e.codigo;

  
  select l.codigo,titulo,autor,nombre,precio
  from libros as l
  join editoriales as e
  on codigoeditorial=e.codigo
  where e.nombre='Siglo XXI';


select titulo,autor,nombre
  from libros as l
  join editoriales as e
  on codigoeditorial=e.codigo
  order by titulo;


select titulo,nombre
  from editoriales as e
  left join libros as l
  on codigoeditorial = e.codigo;


select titulo,nombre
  from libros as l
  left join editoriales as e
  on codigoeditorial = e.codigo;


   select titulo,nombre
  from editoriales as e
  left join libros as l
  on e.codigo=codigoeditorial
  where codigoeditorial is not null;


 select titulo,nombre
  from editoriales as e
  left join libros as l
  on e.codigo=codigoeditorial
  where codigoeditorial is null;

   select nombre as editorial,
  count(*) as cantidad
  from editoriales as e
  join libros as l
  on codigoeditorial=e.codigo
  group by e.nombre;

 select nombre as editorial,
  max(precio) as 'mayor precio'
  from editoriales as e
  left join libros as l
  on codigoeditorial=e.codigo
  group by nombre;
  