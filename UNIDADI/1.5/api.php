<?php
header("Content-Type:application/json");
if (isset($_GET['order_id']) && $_GET['order_id']!="") {
 include('db.php');
 $order_id = $_GET['order_id'];
 $result = mysqli_query(
 $con,
 "SELECT * FROM `transactions` WHERE order_id=$order_id");
 if(mysqli_num_rows($result)>0){
 $row = mysqli_fetch_array($result);
 $amount = $row['amount'];
 $response_code = $row['response_code'];
 $response_desc = $row['response_desc'];
 response($order_id, $amount, $response_code,$response_desc);
 mysqli_close($con);
 }else{
 response(NULL, NULL, 200,"No Record Found");
 }
}else{
 response(NULL, NULL, 400,"Invalid Request");
 }
 
function response($order_id,$amount,$response_code,$response_desc){
 $response['order_id'] = $order_id;
 $response['amount'] = $amount;
 $response['response_code'] = $response_code;
 $response['response_desc'] = $response_desc;
 
 $json_response = json_encode($response);
 echo $json_response;
}

<?php
if (isset($_POST['order_id']) && $_POST['order_id']!="") {
 $order_id = $_POST['order_id'];
 $url = "http://localhost/rest/api/".$order_id;
 
 $client = curl_init($url);
 curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
 $response = curl_exec($client);
 
 $result = json_decode($response);
 
 echo "<table>";
 echo "<tr><td>Order ID:</td><td>$result->order_id</td></tr>";
 echo "<tr><td>Amount:</td><td>$result->amount</td></tr>";
 echo "<tr><td>Response Code:</td><td>$result->response_code</td></tr>";
 echo "<tr><td>Response Desc:</td><td>$result->response_desc</td></tr>";
 echo "</table>";
}

?>