class BDLib
{
	#vars 
	private $host;
    private $us;
    private $pwd;
    private $port; //3306
    private $dbname;
    //var de manejo de acciones de BD
    private $dsn;
    private $dbh;
    private $options;
    //mensaje de error
    public static string $msgError = '';

    public function __constructor($host, $dbname, $us, $pwd, $port = 8889)
    {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->us = $us;
        $this->pwd = $pwd;
        $this->port = $port;
        //datos por default
        if (func_num_args() == 0) {
            $this->host = 'localhost';
            $this->dbname = 'pdv_uth_bd_v1';
            $this->us = 'root';
            $this->pwd = '1234';
        }
        //data spource
        $this->dsn = "$this->protocolo:host=$this->host;port=$this->port;dbname=$this->dbname";
        //optiones para el ERR MODE
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
    }


    public function __construct()
    {
        //se leen los pars de arch ini    // parse_ini_file('../../config/configbd.ini');
        $config = parse_ini_file("../../..app/config/configdb.ini");

        // foreach ($config as $key => $value) {
        //     echo "<br>$key => $value";
        // }

        //se obtienen los datos de arch de config INI
        $this->protocolo = $config['driver'];
        $this->host = $config['host'];
        $this->dbname = $config['dbname'];
        $this->us = $config['us'];
        $this->pwd = $config['pwd'];
        $this->port = $config['port'];
        //data spource
        $this->dsn = "$this->protocolo:host=$this->host;port=$this->port;dbname=$this->dbname";
        //optiones para el ERR MODE
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
    }
        public function abrirConexion()
    {
        try {
            //se abre conexion CREANDO INSTANCIA
            $this->dbh = new PDO($this->dsn, $this->us, $this->pwd, $this->options);
            return true;
        } catch (PDOException $ex) {
            print "Error al conectar " . $ex->getMessage();
            die();
        }
        return false;
    }
